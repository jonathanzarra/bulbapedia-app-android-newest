package org.bulbapedia.useroption.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;

import org.bulbapedia.auth.AccountUtil;
import org.bulbapedia.database.http.HttpStatus;
import org.bulbapedia.useroption.UserOption;
import org.bulbapedia.useroption.database.UserOptionDao;
import org.bulbapedia.useroption.database.UserOptionRow;
import org.bulbapedia.useroption.dataclient.UserInfo;
import org.bulbapedia.useroption.dataclient.UserOptionDataClientSingleton;
import org.bulbapedia.util.log.L;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserOptionSyncAdapter extends AbstractThreadedSyncAdapter {
    public UserOptionSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {
        if (!AccountUtil.supported(account)) {
            L.i("unexpected account=" + account);
            ++syncResult.stats.numAuthExceptions;
            return;
        }

        boolean uploadOnly = extras.getBoolean(ContentResolver.SYNC_EXTRAS_UPLOAD);

        try {
            upload();
            if (!uploadOnly) {
                download();
            }
        } catch (IOException e) {
            L.d(e);
            ++syncResult.stats.numIoExceptions;
        }
    }

    private synchronized void download() throws IOException {
        UserInfo info = UserOptionDataClientSingleton.instance().get();
        Collection<UserOption> options = info.userjsOptions();
        L.i("downloaded " + options.size() + " option(s)");
        UserOptionDao.instance().reconcileTransaction(options);
    }

    private synchronized void upload() throws IOException {
        List<UserOptionRow> rows = new ArrayList<>(UserOptionDao.instance().startTransaction());
        while (!rows.isEmpty()) {
            UserOptionRow row = rows.get(0);

            try {
                if (row.status() == HttpStatus.DELETED) {
                    L.i("deleting user option: " + row.key());
                    UserOptionDataClientSingleton.instance().delete(row.key());
                } else if (!row.status().synced()) {
                    L.i("uploading user option: " + row.key());
                    //noinspection ConstantConditions
                    UserOptionDataClientSingleton.instance().post(row.dat());
                }
            } catch (IOException e) {
                UserOptionDao.instance().failTransaction(rows);
                throw e;
            }

            UserOptionDao.instance().completeTransaction(row);
            rows.remove(0);
        }
    }
}
