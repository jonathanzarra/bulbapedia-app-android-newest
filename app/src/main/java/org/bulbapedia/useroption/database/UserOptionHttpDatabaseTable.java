package org.bulbapedia.useroption.database;

import android.database.Cursor;
import android.support.annotation.NonNull;

import org.bulbapedia.database.async.AsyncTable;
import org.bulbapedia.database.contract.UserOptionContract;
import org.bulbapedia.database.http.HttpRow;
import org.bulbapedia.database.http.HttpStatus;
import org.bulbapedia.useroption.UserOption;

public class UserOptionHttpDatabaseTable extends AsyncTable<HttpStatus, UserOption, HttpRow<UserOption>> {
    private static final int DATABASE_VERSION = 11;

    public UserOptionHttpDatabaseTable() {
        super(UserOptionContract.TABLE_HTTP, UserOptionContract.Http.URI, UserOptionContract.HTTP_COLS);
    }

    @Override public HttpRow<UserOption> fromCursor(@NonNull Cursor cursor) {
        return UserOptionContract.HTTP_COLS.val(cursor);
    }

    @Override protected int getDBVersionIntroducedAt() {
        return DATABASE_VERSION;
    }
}
