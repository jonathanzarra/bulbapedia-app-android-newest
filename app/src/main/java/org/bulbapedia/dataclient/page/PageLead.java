package org.bulbapedia.dataclient.page;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.bulbapedia.dataclient.ServiceError;
import org.bulbapedia.page.Page;
import org.bulbapedia.page.PageTitle;

/**
 * Gson POJI for loading the first stage of page content.
 */
public interface PageLead {
    boolean hasError();

    ServiceError getError();

    void logError(String message);

    /** Note: before using this check that #hasError is false */
    Page toPage(PageTitle title);

    @NonNull String getLeadSectionContent();

    @Nullable String getTitlePronunciationUrl();
    @Nullable String getLeadImageUrl(int leadThumbnailWidth);

    @Nullable Location getGeo();
}
