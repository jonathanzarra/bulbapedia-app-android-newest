package org.bulbapedia.dataclient.page;

import android.support.annotation.NonNull;

import org.bulbapedia.page.Page;
import org.bulbapedia.page.Section;

import java.util.List;

/**
 * Gson POJI for loading remaining page content.
 */
public interface PageRemaining {
    void mergeInto(Page page);
    @NonNull List<Section> sections();
}
