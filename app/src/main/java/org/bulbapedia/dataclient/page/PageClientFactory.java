package org.bulbapedia.dataclient.page;

import android.support.annotation.NonNull;

import org.bulbapedia.dataclient.WikiSite;
import org.bulbapedia.dataclient.mwapi.page.MwPageClient;
import org.bulbapedia.dataclient.mwapi.page.MwPageService;
import org.bulbapedia.dataclient.restbase.page.RbPageClient;
import org.bulbapedia.dataclient.restbase.page.RbPageService;
import org.bulbapedia.dataclient.retrofit.MwCachedService;
import org.bulbapedia.dataclient.retrofit.RbCachedService;
import org.bulbapedia.dataclient.retrofit.WikiCachedService;
import org.bulbapedia.page.Namespace;
import org.bulbapedia.settings.RbSwitch;

/**
 * This redirection exists because we want to be able to switch between the traditional
 * MediaWiki PHP API and the new Nodejs Mobile Content Service hosted in the RESTBase
 * infrastructure.
 */
public final class PageClientFactory {
    @NonNull private static final WikiCachedService<RbPageService> RESTBASE_CACHE
            = new RbCachedService<>(RbPageService.class);
    @NonNull private static final WikiCachedService<MwPageService> MEDIAWIKI_CACHE
            = new MwCachedService<>(MwPageService.class);

    // TODO: remove the namespace check if and when Parsoid's handling of File pages is updated
    // T135242
    public static PageClient create(@NonNull WikiSite wiki, @NonNull Namespace namespace) {
        if (RbSwitch.INSTANCE.isRestBaseEnabled(wiki) && !namespace.file()) {
            return new RbPageClient(RESTBASE_CACHE.service(wiki));
        }
        return new MwPageClient(MEDIAWIKI_CACHE.service(wiki));
    }

    private PageClientFactory() { }
}
