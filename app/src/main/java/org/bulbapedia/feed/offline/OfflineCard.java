package org.bulbapedia.feed.offline;

import android.support.annotation.NonNull;

import org.bulbapedia.feed.model.Card;
import org.bulbapedia.feed.model.CardType;

public class OfflineCard extends Card {
    @NonNull @Override public CardType type() {
        return CardType.OFFLINE;
    }
}
