package org.bulbapedia.feed.progress;

import android.support.annotation.NonNull;

import org.bulbapedia.feed.model.Card;
import org.bulbapedia.feed.model.CardType;

public class ProgressCard extends Card {
    @NonNull @Override public CardType type() {
        return CardType.PROGRESS;
    }
}
