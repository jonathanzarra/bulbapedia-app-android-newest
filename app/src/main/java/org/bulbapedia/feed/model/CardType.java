package org.bulbapedia.feed.model;

import android.content.Context;
import android.support.annotation.NonNull;

import org.bulbapedia.feed.announcement.AnnouncementCardView;
import org.bulbapedia.feed.becauseyouread.BecauseYouReadCardView;
import org.bulbapedia.feed.continuereading.ContinueReadingCardView;
import org.bulbapedia.feed.featured.FeaturedArticleCardView;
import org.bulbapedia.feed.image.FeaturedImageCardView;
import org.bulbapedia.feed.mainpage.MainPageCardView;
import org.bulbapedia.feed.mostread.MostReadCardView;
import org.bulbapedia.feed.news.NewsListCardView;
import org.bulbapedia.feed.offline.OfflineCardView;
import org.bulbapedia.feed.progress.ProgressCardView;
import org.bulbapedia.feed.random.RandomCardView;
import org.bulbapedia.feed.searchbar.SearchCardView;
import org.bulbapedia.feed.view.FeedCardView;
import org.bulbapedia.model.EnumCode;
import org.bulbapedia.model.EnumCodeMap;

public enum CardType implements EnumCode {
    SEARCH_BAR(0) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new SearchCardView(ctx);
        }
    },
    CONTINUE_READING(1) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new ContinueReadingCardView(ctx);
        }
    },
    BECAUSE_YOU_READ_LIST(2) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new BecauseYouReadCardView(ctx);
        }
    },
    MOST_READ_LIST(3) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new MostReadCardView(ctx);
        }
    },
    FEATURED_ARTICLE(4) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new FeaturedArticleCardView(ctx);
        }
    },
    RANDOM(5) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new RandomCardView(ctx);
        }
    },
    MAIN_PAGE(6) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new MainPageCardView(ctx);
        }
    },
    NEWS_LIST(7) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new NewsListCardView(ctx);
        }
    },
    FEATURED_IMAGE(8) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new FeaturedImageCardView(ctx);
        }
    },
    BECAUSE_YOU_READ_ITEM(9),
    MOST_READ_ITEM(10),
    NEWS_ITEM(11),
    NEWS_ITEM_LINK(12),
    ANNOUNCEMENT(13) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new AnnouncementCardView(ctx);
        }
    },
    SURVEY(14) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new AnnouncementCardView(ctx);
        }
    },
    FUNDRAISING(15) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new AnnouncementCardView(ctx);
        }
    },
    OFFLINE(98) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new OfflineCardView(ctx);
        }
    },
    PROGRESS(99) {
        @NonNull @Override public FeedCardView<?> newView(@NonNull Context ctx) {
            return new ProgressCardView(ctx);
        }
    };

    private static final EnumCodeMap<CardType> MAP = new EnumCodeMap<>(CardType.class);
    private final int code;

    @NonNull public static CardType of(int code) {
        return MAP.get(code);
    }

    @NonNull public FeedCardView<?> newView(@NonNull Context ctx) {
        throw new UnsupportedOperationException();
    }

    @Override public int code() {
        return code;
    }

    CardType(int code) {
        this.code = code;
    }
}
