package org.bulbapedia.feed;

import android.content.Context;
import android.support.annotation.NonNull;

import org.bulbapedia.feed.aggregated.AggregatedFeedContentClient;
import org.bulbapedia.feed.announcement.AnnouncementClient;
import org.bulbapedia.feed.becauseyouread.BecauseYouReadClient;
import org.bulbapedia.feed.continuereading.ContinueReadingClient;
import org.bulbapedia.feed.mainpage.MainPageClient;
import org.bulbapedia.feed.random.RandomClient;
import org.bulbapedia.feed.searchbar.SearchClient;

class FeedCoordinator extends FeedCoordinatorBase {

    FeedCoordinator(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void buildScript(int age) {
        if (age == 0) {
            addPendingClient(new SearchClient());
            addPendingClient(new AnnouncementClient());
        }
        addPendingClient(new AggregatedFeedContentClient());
        addPendingClient(new ContinueReadingClient());
        if (age == 0) {
            addPendingClient(new MainPageClient());
        }
        addPendingClient(new BecauseYouReadClient());
        if (age == 0) {
            addPendingClient(new RandomClient());
        }
    }
}
