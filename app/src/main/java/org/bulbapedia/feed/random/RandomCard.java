package org.bulbapedia.feed.random;

import android.support.annotation.NonNull;

import org.bulbapedia.dataclient.WikiSite;
import org.bulbapedia.feed.model.Card;
import org.bulbapedia.feed.model.CardType;

public class RandomCard extends Card {
    @NonNull private WikiSite wiki;

    public RandomCard(@NonNull WikiSite wiki) {
        this.wiki = wiki;
    }

    @NonNull @Override public CardType type() {
        return CardType.RANDOM;
    }

    public WikiSite wikiSite() {
        return wiki;
    }
}
