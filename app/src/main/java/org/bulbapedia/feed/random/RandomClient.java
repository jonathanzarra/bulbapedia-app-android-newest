package org.bulbapedia.feed.random;

import org.bulbapedia.dataclient.WikiSite;
import org.bulbapedia.feed.dataclient.DummyClient;
import org.bulbapedia.feed.model.Card;

public class RandomClient extends DummyClient {
    @Override public Card getNewCard(WikiSite wiki) {
        return new RandomCard(wiki);
    }
}
