package org.bulbapedia.feed.searchbar;

import org.bulbapedia.dataclient.WikiSite;
import org.bulbapedia.feed.dataclient.DummyClient;
import org.bulbapedia.feed.model.Card;

public class SearchClient extends DummyClient {
    @Override public Card getNewCard(WikiSite wiki) {
        return new SearchCard();
    }
}
