package org.bulbapedia.feed.searchbar;

import android.support.annotation.NonNull;

import org.bulbapedia.feed.model.Card;
import org.bulbapedia.feed.model.CardType;

public class SearchCard extends Card {
    @NonNull @Override public CardType type() {
        return CardType.SEARCH_BAR;
    }
}
