package org.bulbapedia.feed.mainpage;

import android.support.annotation.NonNull;

import org.bulbapedia.feed.model.Card;
import org.bulbapedia.feed.model.CardType;

public class MainPageCard extends Card {
    @NonNull @Override public CardType type() {
        return CardType.MAIN_PAGE;
    }
}
