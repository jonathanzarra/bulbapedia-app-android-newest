package org.bulbapedia.feed.mainpage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import org.bulbapedia.R;
import org.bulbapedia.WikipediaApp;
import org.bulbapedia.feed.view.FeedAdapter;
import org.bulbapedia.feed.view.StaticCardView;
import org.bulbapedia.history.HistoryEntry;
import org.bulbapedia.page.PageTitle;

public class MainPageCardView extends StaticCardView<MainPageCard> {
    public MainPageCardView(@NonNull Context context) {
        super(context);
    }

    @Override public void setCard(@NonNull final MainPageCard card) {
        super.setCard(card);
        setTitle(getString(R.string.view_main_page_card_title));
        setSubtitle(getString(R.string.view_main_page_card_subtitle));
        setIcon(R.drawable.icon_feed_today);
    }

    @Override public void setCallback(@Nullable FeedAdapter.Callback callback) {
        super.setCallback(callback);
        setOnClickListener(new CallbackAdapter());
    }

    private class CallbackAdapter implements OnClickListener {
        @NonNull private WikipediaApp app = WikipediaApp.getInstance();

        @Override
        public void onClick(View view) {
            if (getCallback() != null && getCard() != null) {
                PageTitle title = new PageTitle("List_of_Pokémon_by_National_Pokédex_number#Generation_I", app.getWikiSite());
                getCallback().onSelectPage(getCard(),
                        new HistoryEntry(title, HistoryEntry.SOURCE_FEED_MAIN_PAGE));
            }
        }
    }
}
