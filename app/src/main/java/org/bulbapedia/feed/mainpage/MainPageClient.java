package org.bulbapedia.feed.mainpage;

import org.bulbapedia.dataclient.WikiSite;
import org.bulbapedia.feed.dataclient.DummyClient;
import org.bulbapedia.feed.model.Card;

public class MainPageClient extends DummyClient {
    @Override public Card getNewCard(WikiSite wiki) {
        return new MainPageCard();
    }
}
