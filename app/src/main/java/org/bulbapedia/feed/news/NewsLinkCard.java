package org.bulbapedia.feed.news;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import org.bulbapedia.Constants;
import org.bulbapedia.dataclient.WikiSite;
import org.bulbapedia.feed.model.Card;
import org.bulbapedia.feed.model.CardType;
import org.bulbapedia.feed.model.FeedPageSummary;
import org.bulbapedia.page.PageTitle;

import static org.bulbapedia.util.ImageUrlUtil.getUrlForSize;

class NewsLinkCard extends Card {
    @NonNull private FeedPageSummary page;
    @NonNull private WikiSite wiki;

    NewsLinkCard(@NonNull FeedPageSummary page, @NonNull WikiSite wiki) {
        this.page = page;
        this.wiki = wiki;
    }

    @NonNull @Override public String title() {
        return page.getNormalizedTitle();
    }

    @Nullable @Override public String subtitle() {
        return page.getDescription();
    }

    @Nullable @Override public Uri image() {
        String thumbUrl = page.getThumbnailUrl();
        return thumbUrl != null ? getUrlForSize(Uri.parse(thumbUrl), Constants.PREFERRED_THUMB_SIZE) : null;
    }

    @NonNull @Override public CardType type() {
        return CardType.NEWS_ITEM_LINK;
    }

    @NonNull public PageTitle pageTitle() {
        PageTitle title = new PageTitle(page.getTitle(), wiki);
        if (page.getThumbnailUrl() != null) {
            title.setThumbUrl(page.getThumbnailUrl());
        }
        if (!TextUtils.isEmpty(page.getDescription())) {
            title.setDescription(page.getDescription());
        }
        return title;
    }
}
