package org.bulbapedia.page.tabs;

import android.support.annotation.NonNull;

import org.bulbapedia.model.BaseModel;
import org.bulbapedia.page.PageBackStackItem;

import java.util.ArrayList;
import java.util.List;

public class Tab extends BaseModel {
    @NonNull private final List<PageBackStackItem> backStack = new ArrayList<>();

    @NonNull
    public List<PageBackStackItem> getBackStack() {
        return backStack;
    }
}
