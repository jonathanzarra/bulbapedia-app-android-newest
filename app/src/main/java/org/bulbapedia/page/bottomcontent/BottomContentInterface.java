package org.bulbapedia.page.bottomcontent;

import org.bulbapedia.page.PageTitle;

public interface BottomContentInterface {

    void hide();
    void beginLayout();
    PageTitle getTitle();
    void setTitle(PageTitle newTitle);

}
