package org.bulbapedia.onboarding;

public interface OnboardingStateMachine {
    boolean isTocTutorialEnabled();
    void setTocTutorial();
    boolean isSelectTextTutorialEnabled();
    void setSelectTextTutorial();
    boolean isShareTutorialEnabled();
    void setShareTutorial();
    boolean isReadingListTutorialEnabled();
    void setReadingListTutorial();
    boolean isDescriptionEditTutorialEnabled();
    void setDescriptionEditTutorial();
}
