package org.bulbapedia;

public interface BackPressedHandler {
    boolean onBackPressed();
}
