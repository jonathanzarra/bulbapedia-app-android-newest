package org.bulbapedia.model;

import android.support.annotation.NonNull;

public interface EnumStr {
    @NonNull String str();
}
