package org.bulbapedia.model;

public interface EnumCode {
    int code();
}
