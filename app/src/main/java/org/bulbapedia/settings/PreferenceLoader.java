package org.bulbapedia.settings;

/*package*/ interface PreferenceLoader {
    void loadPreferences();
}
