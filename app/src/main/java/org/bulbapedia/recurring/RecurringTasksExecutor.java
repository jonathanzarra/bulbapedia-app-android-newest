package org.bulbapedia.recurring;

import org.bulbapedia.WikipediaApp;
import org.bulbapedia.concurrency.SaneAsyncTask;
import org.bulbapedia.page.snippet.SharedImageCleanupTask;
import org.bulbapedia.settings.RemoteConfigRefreshTask;
import org.bulbapedia.util.ReleaseUtil;

public class RecurringTasksExecutor {
    private final WikipediaApp app;

    public RecurringTasksExecutor(WikipediaApp app) {
        this.app = app;
    }

    public void run() {
        SaneAsyncTask<Void> task = new SaneAsyncTask<Void>() {
            @Override
            public Void performTask() throws Throwable {
                RecurringTask[] allTasks = new RecurringTask[] {
                        // Has list of all rotating tasks that need to be run
                        new RemoteConfigRefreshTask(),
                        new SharedImageCleanupTask(app),
                        new DailyEventTask(app)
                };
                for (RecurringTask task: allTasks) {
                    task.runIfNecessary();
                }
                if (ReleaseUtil.isAlphaRelease()) {
//                    new AlphaUpdateChecker(app).runIfNecessary();
                }
                return null;
            }
        };
        task.execute();
    }
}
