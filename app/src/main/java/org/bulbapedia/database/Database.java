package org.bulbapedia.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.bulbapedia.WikipediaApp;
import org.bulbapedia.edit.summaries.EditSummary;
import org.bulbapedia.history.HistoryEntry;
import org.bulbapedia.pageimages.PageImage;
import org.bulbapedia.readinglist.database.ReadingListRow;
import org.bulbapedia.readinglist.page.ReadingListPageRow;
import org.bulbapedia.savedpages.SavedPage;
import org.bulbapedia.search.RecentSearch;
import org.bulbapedia.useroption.database.UserOptionRow;
import org.bulbapedia.util.log.L;

public class Database extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "wikipedia.db";
    private static final int DATABASE_VERSION = 16;

    private final DatabaseTable<?>[] databaseTables = {
            HistoryEntry.DATABASE_TABLE,
            PageImage.DATABASE_TABLE,
            RecentSearch.DATABASE_TABLE,
            SavedPage.DATABASE_TABLE,
            EditSummary.DATABASE_TABLE,

            // Order matters. UserOptionDatabaseTable has a dependency on
            // UserOptionHttpDatabaseTable table when upgrading so this table must appear before it.
            UserOptionRow.HTTP_DATABASE_TABLE,
            UserOptionRow.DATABASE_TABLE,

            ReadingListPageRow.DISK_DATABASE_TABLE,
            ReadingListPageRow.HTTP_DATABASE_TABLE,
            ReadingListPageRow.DATABASE_TABLE,

            ReadingListRow.DATABASE_TABLE
    };

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        for (DatabaseTable<?> table : databaseTables) {
            table.upgradeSchema(sqLiteDatabase, 0, DATABASE_VERSION);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int from, int to) {
        L.i("Upgrading from=" + from + " to=" + to);
        WikipediaApp.getInstance().putCrashReportProperty("fromDatabaseVersion", String.valueOf(from));
        for (DatabaseTable<?> table : databaseTables) {
            table.upgradeSchema(sqLiteDatabase, from, to);
        }
    }
}
