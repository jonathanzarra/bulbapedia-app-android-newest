package org.bulbapedia.captcha;

import android.support.annotation.NonNull;

import org.bulbapedia.dataclient.mwapi.MwResponse;

public class Captcha extends MwResponse {
    @SuppressWarnings("unused,NullableProblems") @NonNull private FancyCaptchaReload fancycaptchareload;
    @NonNull String captchaId() {
        return fancycaptchareload.index();
    }

    private static class FancyCaptchaReload {
        @SuppressWarnings("unused,NullableProblems") @NonNull private String index;
        @NonNull String index() {
            return index;
        }
    }
}
