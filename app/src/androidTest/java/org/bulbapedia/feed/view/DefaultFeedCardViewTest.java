package org.bulbapedia.feed.view;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.Theory;
import org.bulbapedia.feed.model.Card;
import org.bulbapedia.feed.view.FeedAdapter.Callback;
import org.bulbapedia.test.theories.TestedOnBool;
import org.bulbapedia.test.view.FontScale;
import org.bulbapedia.test.view.LayoutDirection;
import org.bulbapedia.test.view.ViewTest;
import org.bulbapedia.theme.Theme;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;

public class DefaultFeedCardViewTest extends ViewTest {
    private DefaultFeedCardView<Card> subject;

    @Before public void setUp() {
        setUp(WIDTH_DP_S, LayoutDirection.LOCALE, FontScale.DEFAULT, Theme.LIGHT);
        subject = new Subject(ctx());
    }

    @Test public void testSetGetCard() {
        Card card = mock(Card.class);
        subject.setCard(card);
        assertThat(subject.getCard(), is(card));
    }

    @Theory public void testSetGetCallback(@TestedOnBool boolean nul) {
        Callback callback = nul ? null : mock(Callback.class);
        subject.setCallback(callback);
        assertThat(subject.getCallback(), is(callback));
    }

    private static class Subject extends DefaultFeedCardView<Card> {
        Subject(Context context) {
            super(context);
        }
    }
}
