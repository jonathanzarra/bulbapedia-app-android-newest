package org.bulbapedia.json;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.ParameterizedRobolectricTestRunner;
import org.robolectric.ParameterizedRobolectricTestRunner.Parameters;
import org.bulbapedia.Constants;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.bulbapedia.json.GsonMarshaller.marshal;
import static org.bulbapedia.json.GsonUnmarshaller.unmarshal;

@RunWith(ParameterizedRobolectricTestRunner.class) public class UriTypeAdapterTest {
    @Parameters(name = "{0}") public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][] {{DeferredParam.NULL}, {DeferredParam.STRING},
                {DeferredParam.OPAQUE}, {DeferredParam.HIERARCHICAL}});
    }

    @Nullable private final Uri uri;

    public UriTypeAdapterTest(@NonNull DeferredParam param) {
        this.uri = param.val();
    }

    @Test public void testWriteRead() {
        Uri result = unmarshal(Uri.class, marshal(uri));
        assertThat(result, is(uri));
    }

    // Namespace uses a roboelectric mocked class internally, SparseArray, which is unavailable at
    // static time; defer evaluation until TestRunner is executed
    private enum DeferredParam {
        NULL() {
            @Nullable @Override Uri val() {
                return null;
            }
        },
        STRING() {
            @Nullable @Override Uri val() {
                return Uri.parse(Constants.WIKIPEDIA_URL);
            }
        },
        OPAQUE() {
            @Nullable @Override Uri val() {
                return Uri.fromParts("http", "mediawiki.org", null);
            }
        },
        HIERARCHICAL() {
            @Nullable @Override Uri val() {
                return Uri.EMPTY;
            }
        };

        @Nullable abstract Uri val();
    }
}
