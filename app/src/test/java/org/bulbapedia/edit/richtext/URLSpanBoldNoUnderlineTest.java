package org.bulbapedia.edit.richtext;

import android.os.Parcelable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.bulbapedia.richtext.URLSpanBoldNoUnderline;
import org.bulbapedia.test.TestParcelUtil;
import org.bulbapedia.test.TestRunner;

@RunWith(TestRunner.class) public class URLSpanBoldNoUnderlineTest {
    @Test public void testCtorParcel() throws Throwable {
        Parcelable subject = new URLSpanBoldNoUnderline("url");
        TestParcelUtil.test(subject);
    }
}
