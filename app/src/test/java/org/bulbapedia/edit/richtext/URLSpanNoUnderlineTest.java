package org.bulbapedia.edit.richtext;

import android.os.Parcelable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.bulbapedia.richtext.URLSpanNoUnderline;
import org.bulbapedia.test.TestParcelUtil;
import org.bulbapedia.test.TestRunner;

@RunWith(TestRunner.class) public class URLSpanNoUnderlineTest {
    @Test public void testCtorParcel() throws Throwable {
        Parcelable subject = new URLSpanNoUnderline("url");
        TestParcelUtil.test(subject);
    }
}
